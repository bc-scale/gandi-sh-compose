# gandi-sh-compose

A Docker Compose setup to replicate as much as possible
the Gandi Simple Hosting environment.

## Usage

```shell
docker compose --env-file ./.env-file up -d
```

## Documentation

The full documentation from the [`docs/`](docs/index.md) directory is published at
<https://bibliosansfrontieres.gitlab.io/infrastructure/gandi/gandi-sh-compose>.
