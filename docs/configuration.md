# Configuration

The docker-compose file expects some environment variables to be set.

The Compose setup expects these variables ot be set in a
[env file](https://docs.docker.com/compose/env-file/).

Real world example of such an env file can be found in the [workflows](workflows.md) page.

## Configuration variables

### PHP_IMAGE

Choose any `-apache` tag, among the versions Gandi provides. Note that the
[`gandi-sh-docker`](https://bibliosansfrontieres.gitlab.io/infrastructure/gandi/gandi-sh-docker)
project provides a Docker image which matches as close as possible the Gandi Simple Hosting environment.

Default: `php:7.4-apache`

### MYSQL_IMAGE

Choose among the versions Gandi provides

Default: `mysql:5.7`

### MYSQL_DATABASE

Mandatory, must match the one as configured in your webapp.

Default: (no default)

### MYSQL_USER

Mandatory, must match the one as configured in your webapp.

Default: (no default)

### MYSQL_PASSWORD

Mandatory, must match the one as configured in your webapp.

Default: (no default)

### DOCUMENTROOT_PATH

Mandatory.

This is the `DocumentRoot` that will be served by the `$PHP_IMAGE` container.

The `restore.sh` script will copy files from `$GSH_BACKUPS_ROOT/$GSH_GSH_REMOTE_VHOST/current/files/htdocs/`,
and delete the files that were created afterward.

Default: (no default)

### SQL_DUMP_FILE

Mandatory.

This is the path to the MySQL database dump file
the MySQL container shall load to init the database.

Default: (no default) 

## Overrides

The default values - if any - are overriden
by the values from the `.env` file.
In turn, these values from the `.env` file may be overriden
by the ones provided from the CLI.

Consider this example:

```shell
$ fgrep PHP_IMAGE .env-file
PHP_IMAGE=php:7.3-apache
$ PHP_IMAGE=php:8.1-apache docker-compose --env-file .env-file docker-compose config | egrep image:.*php
    image: php:8.1
```
