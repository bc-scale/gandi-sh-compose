# gandi-sh-compose

A Docker Compose setup to replicate as much as possible
the Gandi Simple Hosting environment.

## Overview

The stack is a simple database/webserver setup.

It is designed to be provisioned from a
[`gandi-sh-backup`](https://bibliosansfrontieres.gitlab.io/infrastructure/gandi/gandi-sh-backup)
based backup.

This allows the [configuration](configuration) to be shared across `gandi-sh-*` tools.

