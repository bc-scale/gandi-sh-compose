# Domains migration

Local development is handy, but the stack also allows to be publicly accessible.

This allows to show your work, or to provide for a testing environment.

In such case, a public FQDN is required.

In order to not conflict with the production FQDN, the `migrate-domains.sh` script
may be used to perform a basic search'n'replace dance in the Wordpress configuration.

## Configuration

The `CUSTOM_DOMAIN` environment variable must be set in the `.env` file ;
the variable value is the domain that will be prepended to your production FQDN.
For example, if your production blog lives at `myblog.org` and you set `CUSTOM_DOMAIN` to `example.com`,
the script will make the stack available at <https://myblog.org.example.com>.

## DNS

You need a DNS record matching `CUSTOM_DOMAIN` to point at your instance.

## Domain migration

Run the `migrate-domains.sh` script:

* `wp-config.php` and `.htaccess` files are patched
* the database is search'n'replaced as a whole

## Disclaimer

Here again, the stack is not iso anymore with the production.
