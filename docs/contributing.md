# Contributing

## Roadmap

These quick notes shall be turned into proper issues.

* ❌ The `max_allowed_packets.cnf` file seems to not be considered by the `mysql` container
* ❌ Better UID/GID handling (your `1001` host-side vs `www-data`/`mysql` container-side)
* 💡 Stick closer to MySQL configuration
* 💡 (optional) domain(s) mapping: `s/production.tld/sandbox.tld/ db.sql`
