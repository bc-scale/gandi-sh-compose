# Workflows

Here are some real world example of stack usage.

We are willing to work on the `www.bibliosansfrontieres.org` website.

## Local development from scratch

We don't want to work on an existing website, but rather want to start a new one from scratch,
making sure we work as close as possible with the Gandi Simple Hosting environment.

* Configure `MYSQL_IMAGE` and `PHP_IMAGE` to match the target SH instance type
* Start the stack: `docker compose --env-file .env up -d`
* Work inside the `htdocs/` directory

## Local development from an existing website

We want to work on an existing website, but we don't want to break our production website.

We need to provide for the PHP files and the database dump. The [`gandi-sh-backup`](https://bibliosansfrontieres.gitlab.io/infrastructure/gandi/gandi-sh-backup) is an easy way and allows for the configuration variables to be reused, but this is not mandatory ; a manual (files/DB) dump would work the same.

### The env file

We will use the following `env` file, named `.env.www.bibliosansfrontieres.org`:

```bash
# backups
GSH_BACKUPS_ROOT=/home/tom/dev/webdev/gandi-sh-backup
GSH_GSH_RCLONE_REMOTE=gandi-sh-bsf-org
GSH_GSH_RCLONE_CMD="rclone -v -v"
GSH_GSH_REMOTE_VHOST=www.bibliosansfrontieres.org

# compose
PHP_IMAGE=local/gandi-sh-docker/gandi-sh:php7.4

MYSQL_IMAGE=mysql:5.7
MYSQL_DATABASE=bsforg
MYSQL_USER=bsfdbuser
MYSQL_PASSWORD=bsfdbpassword

DOCUMENTROOT_PATH=./htdocs
SQL_DUMP_FILE=./bsforg.sql
```

### Make a backup


We take for granted we initially used the [`gandi-sh-terraform`](https://bibliosansfrontieres.gitlab.io/infrastructure/gandi/gandi-sh-terraform)
tool in order to install the database backup script.

Use the [`gandi-sh-backup`](https://bibliosansfrontieres.gitlab.io/infrastructure/gandi/gandi-sh-backup) script
to perfom a backup.

As a result:

* a complete dump of the files is available at
  `/home/tom/dev/webdev/gandi-sh-backup/www.bibliosansfrontieres.org/current/files/htdocs/`
* a database dump (gzipped) is available at
  `/home/tom/dev/webdev/gandi-sh-backup/www.bibliosansfrontieres.org/current/db/bsforg-20221102-001923.sql.gz`

### Files provisioning

The `restore.sh` script usage is as follow:

```
    files   restore the PHP files from source (requires sudo)
    db      restore the database dump from source
    all     restore both DB and files (default)
```

Since it is our first run, we want it `all`:

```bash
./restore.sh all
```

As a result:

* files were copied to `$DOCUMENTROOT_PATH`, which is `./htdocs/`
* the database dump is uncompressed at `$SQL_DUMP_FILE`, which is `./bsforg.sql`

### Run

```bash
docker compose --env-file .env.www.bibliosansfrontieres.org up -d
```

Since this Wordpress is a multisite instance, I built a domains list
from which the `build-etchosts-file.sh` builds a custom `/etc/hosts` entry:

```
$ ./build-etchosts-file.sh | sed -e 's/\. / /g'
[+] 2022-11-21 11:06:49 Copy this to your /etc/hosts file:
127.0.0.1  bibliosansfrontieres.org www.bibliosansfrontieres.org bibliosansfrontieres.be www.bibliosansfrontieres.be bibliosansfrontieres.ch www.bibliosansfrontieres.ch bibliotechesenzafrontiere.ch www.bibliotechesenzafrontiere.ch bibliotechesenzafrontiere.it www.bibliotechesenzafrontiere.it bibliothekenohnegrenzen.ch www.bibliothekenohnegrenzen.ch bibliothekenzondergrenzen.be www.bibliothekenzondergrenzen.be bibliothekenzondergrenzen.org www.bibliothekenzondergrenzen.org bibzondergrenzen.be www.bibzondergrenzen.be librarieswithoutborders.ch www.librarieswithoutborders.ch librarieswithoutborders.org www.librarieswithoutborders.org librarieswithoutborders.us www.librarieswithoutborders.us lwb2020us.bibliosansfrontieres.org librarieswithoutborders2020us.bibliosansfrontieres.org www.bibliosansfrontieres.ca www.librarieswithoutborders.ca
```

Using a hardcoded domains list is quite fragile, but it turns out all domains are NOT configured in Wordpress. :-(

### Profit

From now on, the website is available at its regular FQDN.

Remember to remove (or comment) the `/etc/hosts` entry when you want to visit the production website!

## Remote development from an existing website

We want to provide a sandbox environment for a remote team to work on the website.

In order to not clash with the production domain name, we will use `wp.ideas-box.cc` as a subdomain, holding all our FQDNs.

This way, our remote team can see from the URL that they are NOT working on the production website, still having explicit URLs matching the subsite they want to work on.

### Install server side requirements

We are using a remote server (AWS, OVH, ...) on which we must [install docker](https://docs.docker.com/engine/install/).

Our server is `bsforg-sandbox.bsf-intranet.org`.

### Add a DNS entry

Add a `CUSTOM_DOMAIN` to your `.env.www.bibliosansfrontieres.org` file:

```bash
CUSTOM_DOMAIN=ideas-box.cc
```

Add a DNS entry which points to the server.

Since we need to serve multiple FQDNs, we need a wildcard `CNAME`:

```text
*.wp    CNAME    600    bsforg-sandbox.bsf-intranet.org.
```

### Provision the instance

From there, apply these steps from above:

* The env file
* Make a backup
* Files provisioning

### Domain migration

Run the `migrate-domains.sh` script.

This will basically search for domains configured in Wordpress, and add `.wp-ideas-box.cc` to every match.

It will do the same in the `wp-config.php` file in order to reconfigure the "main" site.

It will also patch the `.htaccess` file, should you have `RewriteRules` in it.

As a result:

* Our main website is available at <https://www.bibliosansfrontieres.org.wp.ideas-box.cc>
* Subsites are available at <https://www.librarieswithoutborders.us.wp.ideas-box.cc> and the like

## A developer's life

How to go back and forth states.

### Restoring files

You tried things around but you are not happy with them and want to start from scratch back from the backups.

The usual `./restore.sh files` invocation will reset your `$DOCUMENTROOT_PATH` to match your last backup from `$GSH_BACKUPS_ROOT/$GSH_GSH_REMOTE_VHOST/current/files/htdocs/`

Remeber that your past attempts may have modified the database, which at this point doesn't match the backup one.

### Restoring database

So you probably want to restore the database as well.

The dump from the backups `$SQL_DUMP_FILE` is used to provision the database at container creation, so we can just stop the database container, delete the database files, and restart the stack:

```
docker compose stop web-db
rm -rf ./db-data
docker compose up -d
```

The database container is restarted, and since its database is empty it will be provisioned from the `$SQL_DUMP_FILE` file.

A quicker way is to use `mysqldump(1)` to import the SQL dump into the database. For this you need `mysqldump(1)` to be available on your local shell. We need a few additionals parameters in order to communicate with the `mysqld` daemon:

```bash
mysql -h localhost -P 3306 --protocol=tcp -u"$MYSQL_USER" -p"$MYSQL_PASSWORD" "$MYSQL_DATABASE" < "$SQL_DUMP_FILE"
```

After restoring the database dump, if working remote, you need to run the `migrate-domains.sh` script again.
