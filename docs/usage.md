# Usage

```shell
docker-compose --env-file ./.env-file up -d
```

### MySQL

In order to replicate the SH instance, you'd have to tweak the `max_allowed_packet` value
to match the target instance size:

|  Instance type | `max_allowed_packet` |
| -------------- | -------------------- |
|  S+ | 16M |
|  M | ? |
|  L | ? |
|  XXL | 64M |

### DNS

You probably want to edit your local `/etc/hosts` file
in order to point the website's FQDN to `localhost`:

```shell
echo '127.0.0.1 www.example.com' | sudo tee -a /etc/hosts
```
