#!/bin/bash


# TODO
# - get domains from DB, not from dev/domains.txt file
# - conditional CUSTOM_DOMAIN append (i.e. only if migrate-domains.sh was ran)



say() {
    >&2 echo "[+] $( date '+%F %T') $*"
}

# shellcheck disable=SC1091
. ./.env.bsf-org

fqdn_list=""
while read -r ndd ; do
    # shellcheck disable=SC2001
    this_fqdn=$( echo "$ndd" | sed -e 's/$/.'"${CUSTOM_DOMAIN}"'/' )
    fqdn_list="$fqdn_list $this_fqdn"
done < dev/domains.txt


say "Copy this to your /etc/hosts file:"
echo "127.0.0.1 $fqdn_list"
