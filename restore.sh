#!/bin/bash

# shellcheck disable=SC1091
. ./.env.bsf-org

show_usage() {
    cat <<EOF
Usage: $(basename "$0" ) [ARG]

ARG can be:
    files   restore the PHP files from source (requires sudo)
    db      restore the database dump from source
    all     restore both DB and files (default)
EOF
}

say() {
    >&2 echo "[+] $( date '+%F %T') $*"
}

restore_files() {
    say "Restore files..."
    sudo chown "$( whoami )" -R "$DOCUMENTROOT_PATH"
    time rsync -a --delete "${GSH_BACKUPS_ROOTDIR}/files/htdocs/" "$DOCUMENTROOT_PATH"
    sudo chown www-data -R "$DOCUMENTROOT_PATH"
}
restore_db() {
    say "Restore database..."
    # shellcheck disable=SC2012
    latestdb="$( ls -1 "${GSH_BACKUPS_ROOTDIR}"/db/bsf* -tr | tail -n 1 )"
    rm -f "$SQL_DUMP_FILE"
    time zcat "$latestdb" > "$SQL_DUMP_FILE"
    chmod a+r "$SQL_DUMP_FILE"
}
restore_all() {
    restore_files
    restore_db
}

what=${1:-all}

[[ $what == "h" || $what == "--help" ]] && {
    show_usage
    exit 0
}

case $what in
    files|db|all)
        "restore_${what}"
        ;;
    *)
        echo >&2 "Error: unknown arg: $what"
        show_usage
        exit 2
        ;;
esac

