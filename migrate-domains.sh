#!/bin/bash

# MySQL 8+ has REGEX_REPLACE https://dev.mysql.com/doc/refman/8.0/en/regexp.html#function_regexp-replace
# so we could basically match against /$domain[^\.]/

# TODO
# - requires to bind the MySQLd port to host / hardcoded host/port
#   -> check whether/how to use a `docker exec` based script

# shellcheck disable=SC1091
. ./.env.bsf-org

say() {
    >&2 echo "[+] $( date '+%F %T') $*"
}

query() {
    mysql -h localhost -P 3306 --protocol=tcp \
        -u "$MYSQL_USER" -p"${MYSQL_PASSWORD}" "$MYSQL_DATABASE" \
        --skip-column-names \
        -e "$@"
}

say "Waiting for mysql..."
until query "SELECT 1" &> /dev/null ; do
  printf "."
  sleep 1
done
printf "\n"

set -euo pipefail

say "Update domains in database..."
query "SELECT domain FROM BSF_blogs" \
    | sed -e 's/www\.//' \
    | while read -r domain ; do
        # FIXME: this is done in two passes
        # I tried to use regex but with or without `--precise` they are greedy in a weird way

        say ":: search-replace ${domain}..." # FAIL on Elementor based sites \o/ .be
        time docker exec -t web-app wp search-replace "${domain}" "${domain}.${CUSTOM_DOMAIN}" \
            --report-changed-only --skip-columns=guid,option_name --all-tables --skip-tables='*_users,*_itsec_*'
        say ":: fix emails for ${domain}..."
        time docker exec -t web-app wp search-replace "@${domain}.${CUSTOM_DOMAIN}" "@${domain}" \
            --report-changed-only --skip-columns=guid,option_name --all-tables --skip-tables='*_users,*_itsec_*'
        # while we're on it ; fix paths?
    done

main_domain="$( query "SELECT domain FROM BSF_site WHERE id = '1' ; " )"

say "Update wp-config.php..."
sudo sed -i -e "/DOMAIN_CURRENT_SITE/s/${main_domain}/${main_domain}.${CUSTOM_DOMAIN}/" htdocs/wp-config.php
sudo sed -i -e "/NOBLOGREDIRECT/s/${main_domain}/${main_domain}.${CUSTOM_DOMAIN}/" htdocs/wp-config.php

say "Update .htaccess rules..."
sudo sed -i -e "s/bibliosansfrontieres.org/bibliosansfrontieres.org.${CUSTOM_DOMAIN}/" htdocs/.htaccess
sudo sed -i -e "s/bibliosansfrontieres.be/bibliosansfrontieres.be.${CUSTOM_DOMAIN}/" htdocs/.htaccess
